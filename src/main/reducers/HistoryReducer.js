import immutable from 'seamless-immutable';

import {HistoryType} from '../actions/ActionTypes';

const INITIAL_STATE = immutable({
    history: undefined
});


export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case HistoryType.GET_HISTORY_SUCCESS:
            return state.merge({data: action.response});
        default:
            return state
    }
}