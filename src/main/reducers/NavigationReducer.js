import immutable from 'seamless-immutable';

import { LoginType, NavigationType }  from '../actions/ActionTypes';

const INITIAL_STATE = immutable({
    selectedBody: 'permitView',
    travelPossible: true
});


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case NavigationType.PERMITVIEW_BUTTON_PRESSED:
            return state.merge({ selectedBody: 'permitView' });

        case NavigationType.HOMEVIEW_BUTTON_PRESSED:
            return state.merge({ selectedBody: 'homeView' });

        case NavigationType.MY_DATAVIEW_BUTTON_PRESSED:
            return state.merge({ selectedBody: 'mydataView' });

        case NavigationType.PERMIT_OVERVIEW_BUTTON_PRESSED:
            return state.merge({ selectedBody: 'permitOverview' });

        case NavigationType.ABOUT_BUTTON_VIEW_PRESSED:
            return state.merge({ selectedBody: 'aboutView' });

        case NavigationType.HISTORY_BUTTON_VIEW_PRESSED:
            return state.merge({ selectedBody: 'historyView' });

        case LoginType.LOGOUT_BUTTON_PRESSED:
            return state.merge({ selectedBody: 'permitView' });

        default:
            return state
    }
}