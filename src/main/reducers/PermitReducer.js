import immutable from 'seamless-immutable';

import {PermitType} from '../actions/ActionTypes';
import {AsyncStorage} from 'react-native';

const INITIAL_STATE = immutable({
    permits: undefined,
    isLoading: true,
    processing: false
});


export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case PermitType.GET_PERMITS_SUCCESS:
            return state.merge({permits: action.response.data, isLoading: false});
        case PermitType.START_PERMIT_PROCESS:
            return state.merge({processing: true});
        case PermitType.STOP_PERMIT_PROCESS:
            return state.merge({processing: false});
        default:
            return state
    }
}