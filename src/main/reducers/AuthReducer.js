import {AuthTypes} from '../actions/ActionTypes';

function auth(state = {}, action){
    switch(action.type){
        // saves the token into the state
        case AuthTypes.AUTH_SET_TOKEN:
            return {
                ...state,
                token: action.token
            };
        // discards the current token (logout)
        case AuthTypes.AUTH_DISCARD_TOKEN:
            return {};
        // saves the current user
        case AuthTypes.AUTH_SET_USER:
            return {
                ...state,
                user
            };
        // as always, on default do nothing
        default:
            return state;
    }
}

module.exports = auth;