import immutable from 'seamless-immutable';

import { LoginType } from '../actions/ActionTypes';
import { AsyncStorage } from 'react-native';

import {deleteItem} from '../utils/StorageWrapper';

const INITIAL_STATE = immutable({
    loggedIn: false
});


export default function(state = INITIAL_STATE, action) {
    switch(action.type) {

        case LoginType.LOGIN_SUCCESS:
            return state.merge({loggedIn: true, user: action.response});

        case LoginType.SET_TOKEN:
            AsyncStorage.setItem('userToken', action.token);
            return state;

        case LoginType.GET_TOKEN_FROM_STORAGE_SUCCESS:
            return state.merge({loggedIn: true});


        case LoginType.LOGOUT_BUTTON_PRESSED:
            deleteItem('token');
            return state.merge({loggedIn: false, user: null});

        default:
            return state

    }
}