import immutable from 'seamless-immutable';
import {ListView} from 'react-native';

import {UserType} from '../actions/ActionTypes';

const INITIAL_STATE = immutable({
    isLoading: true
});

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case UserType.GET_USER_DATA_SUCCESS: {
            return state.merge({user: action.response, isLoading: false, loggedIn: true})
        }
        case UserType.ADDED_NEW_CAR:{
            return state.merge({userHasCar: true});
        }
        case UserType.UPDATED_CAR:{
            return state.merge({userHasCar: true});
        }
        default: {
            return state
        }
    }
}