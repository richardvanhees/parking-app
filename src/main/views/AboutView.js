import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Image, StyleSheet, Text, ScrollView} from 'react-native';
import images from '../elements/GlobalImages';

import globalStyles from '../elements/GlobalStyles';

const styles = {
    logo: {
        alignSelf: 'center'
    },
    logoCaption: {
        marginTop: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    section: {
        marginTop: 50,
    },
    textItem:{
        textAlign: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        lineHeight: 20
    },
    textBlock: {
        marginTop: 20,
    },
    textHeader:{
        fontWeight: 'bold'
    },
    textAppVersion:{
        fontStyle: 'italic'
    },
    textItemSmall:{
        fontSize: 11
    }
};

class AboutView extends Component {
    constructor(props) {
        super(props);

    }

    shouldComponentUpdate() {
        return false;
    }


    render() {
        return (
            <View style={{flex: 1}}>
                    <View style={styles.container}>
                        <Image source={images.acato.src}
                               style={styles.logo}
                               width={images.acato.size[0]}
                               height={images.acato.size[1]}/>
                        <Text style={styles.logoCaption}>
                            Powered by acato
                        </Text>

                        <View style={styles.section}>
                            <View style={styles.textBlock}>
                                <Text style={[styles.textHeader, styles.textItem]}>
                                    Account leads
                                </Text>
                                <Text style={styles.textItem}>
                                    Grietje Bruijntjes || Valentijn de Jong{"\n"}Sophie Lotgering
                                </Text>
                            </View>
                            <View style={styles.textBlock}>
                                <Text style={[styles.textHeader, styles.textItem]}>
                                    Design
                                </Text>
                                <Text style={styles.textItem}>
                                    Andrée Lange || Rick Egidius
                                </Text>
                            </View>
                            <View style={styles.textBlock}>
                                <Text style={[styles.textHeader, styles.textItem]}>
                                    Development
                                </Text>
                                <Text style={styles.textItem}>
                                    Davy Obdam || Richard van Hees
                                </Text>
                            </View>
                        </View>

                        <View style={styles.section}>
                            <Text style={[styles.textAppVersion, styles.textItem, styles.textItemSmall]}>
                                ParkeerApp
                            </Text>
                            <Text style={[styles.textItem, styles.textItemSmall]}>
                                Versie 1.2.0
                            </Text>
                        </View>
                    </View>
            </View>
        )
    }

}

export default AboutView;