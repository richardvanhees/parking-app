import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, ScrollView, Text, StyleSheet} from 'react-native';

import Header from '../components/Header';
import Licence from '../elements/Licence';
import globalStyles from '../elements/GlobalStyles';

const styles = StyleSheet.create({
    container: {
        marginBottom: 0,
        marginTop: 15,
        padding: 0
    },
    containerFirst: {
        marginTop: 25
    },
    containerLast: {
        marginBottom: 25
    },
    leftTimeContainer: {
        borderRightWidth: 4,
        borderColor: 'white'
    },
    licenceBlock: {
        marginLeft: -10,
        marginRight: -10,
        flex: 1,
        flexDirection: 'row'
    },
    licenceContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    licenceNumber:{
        lineHeight: 16,
        fontSize: 12,
        marginTop: 0,
        marginBottom: 0
    },
    timeBlock: {
        marginTop: 20,
        marginLeft: -10,
        marginRight: -10,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#e6ebf5',
    },
    time: {
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 26,
        color: 'black'
    },
    textTop: {
        fontSize: 10,
        paddingBottom: 5
    },
    timeText: {
        textAlign: 'center',
        fontSize: 14
    },
    timeContainer: {
        flex: 1,
        paddingTop: 5,
        paddingBottom: 10
    }
});

class HistoryView extends Component {
    constructor(props) {
        super(props);

    }

    shouldComponentUpdate() {
        return true;
    }

    componentWillReceiveProps(nextProps) {

    }

    _formatDate(timeStamp, format) {
        let date = new Date(timeStamp * 1000),
            minutes, time, fullDate, dateString;

        switch(format) {
            case 'time':
                minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
                dateString = `${date.getHours()}:${minutes}`;
                break;
            case 'date':
                dateString = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
                break;
        }


        return dateString;
    }

    render() {
        let historyItems = [],
            history = this.props.history.data,
            minutes,
            time,
            fullDate;

        for(let i = 0; i < history.length; i++) {
            let item = history[i];

            // Set styles
            let itemStyle = [globalStyles.mainContent, styles.container];
            if(!i) itemStyle.push(styles.containerFirst);
            else if(i == (history.length - 1)) itemStyle.push(styles.containerLast);

            console.log(item);
            historyItems.push((
                <View key={item.started} style={itemStyle}>
                    <View style={styles.licenceBlock}>
                        <View style={styles.licenceContainer}>
                            <Licence inline={true} plate={item.car.licence}/>
                        </View>
                        <View style={styles.licenceContainer}>
                            <Text style={[globalStyles.mainContentHeaderSubtitle, styles.licenceNumber]}>Vergunning</Text>
                            <Text style={[globalStyles.mainContentHeaderSubtitle, styles.licenceNumber]}>{item.permit.name}</Text>
                        </View>
                    </View>

                    <View style={styles.timeBlock}>
                        <View style={[styles.timeContainer, styles.leftTimeContainer]}>
                            <Text style={[styles.timeText, styles.textTop]}>van</Text>
                            <Text style={[styles.timeText, styles.time]}>{this._formatDate(item.started, 'time')}</Text>
                            <Text style={styles.timeText}>{this._formatDate(item.started, 'date')}</Text>
                        </View>
                        <View style={styles.timeContainer}>
                            <Text style={[styles.timeText, styles.textTop]}>tot</Text>
                            <Text style={[styles.timeText, styles.time]}>{this._formatDate(item.ended, 'time')}</Text>
                            <Text style={styles.timeText}>{this._formatDate(item.ended, 'date')}</Text>
                        </View>
                    </View>
                </View>));
        }

        return (
            <View style={{flex: 1}}>
                <Header title="Geschiedenis"/>
                <ScrollView style={globalStyles.mainView}>
                    {historyItems}
                </ScrollView>
            </View>

        )
    }
}

function mapStateToProps(state) {
    console.log('HISTORY:', state);
    return {
        user: state.user.user,
        history: state.history
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryView);