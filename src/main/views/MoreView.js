import React, {Component} from 'react';
import {StackNavigator} from 'react-navigation';
import MyDataView from '../views/MyDataView';
import HistoryView from '../views/HistoryView';
import CarView from '../views/CarView';
import AboutView from '../views/AboutView';

import globalStyles, {primaryColor} from '../elements/GlobalStyles';

const navigationOptions = {
    headerTitleStyle: globalStyles.titleText,
    headerStyle: globalStyles.stackHeader,
    headerBackTitle: null,
    headerTintColor: primaryColor,
};

const MoreViewNavigator = StackNavigator({
    MyDataView: {
        screen: MyDataView,
        navigationOptions: {
            ...navigationOptions,
            title: "Instellingen"
        }
    },
    // Next version
    /*HistoryView:{
        screen: HistoryView,
        navigationOptions: {
            ...navigationOptions,
            title: "Geschiedenis"
        }
    },*/
    CarView:{
        screen: CarView,
        navigationOptions: {
            ...navigationOptions,
            title: "Mijn auto"
        }
    },
    AboutView:{
        screen: AboutView,
        navigationOptions: {
            ...navigationOptions,
            title: "Over deze app"
        }
    }
}, {
    initialRouteName: 'MyDataView'
});

export default class MoreView extends Component{
    render(){
        return(
            <MoreViewNavigator />
        );
    }
}