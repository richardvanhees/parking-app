import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, RefreshControl, Image, ScrollView, StyleSheet, Text, TextInput, Button, KeyboardAvoidingView, Alert} from 'react-native';
import {StackNavigator} from 'react-navigation';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import {logoutButtonPressed} from '../actions/LoginActions';

import globalStyles from '../elements/GlobalStyles';
import Header from '../components/Header';
import SolidButton from '../elements/SolidButton';
import {Category, Item, ButtonCategory, ButtonItem} from "../elements/Settings";

const styles = {
    container: {
        padding: 0,
        marginTop: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        paddingLeft: 10,
        paddingTop: 15,
        paddingBottom: 5,
        paddingRight: 5,
        margin: 0,
        fontSize: 12,
    },
    headerSecond: {
        paddingTop: 50
    },
    input: {
        fontSize: 16,
        lineHeight: 45,
        height: 45
    },
    value: {
        height: 36,
        lineHeight: 36,
        color: '#8e8e8e',
        textAlign: 'right',
        flex: 80
    },
    label: {
        height: 36,
        lineHeight: 36,
        flex: 20
    }

};

class MyDataView extends Component {
    state = {
        user: null
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user
        });
    }

    render() {
        let {logoutButtonPressed, navigation} = this.props;
        let {user} = this.state;

        return this.state.user ? (
            <View style={{flex: 1}}>
                <KeyboardAwareScrollView
                    getTextInputRefs={() => {
                        return [this._textInputRef];
                    }}
                    style={globalStyles.mainView}
                >
                    <View style={globalStyles.mainContent}>

                        <Category title="MIJN GEGEVENS">
                            <Item label="Naam">{user.profile.name_full}</Item>
                            <Item label="E-mail">{user.email}</Item>
                            <Item label="Bedrijf">{user.company.name}</Item>
                            <Item label="Locatie">{user.location.name}</Item>
                            <Item label="Kenteken">{!!user.cars.data.length ? user.cars.data[0].licence : ''}</Item>
                        </Category>


                        <ButtonCategory>
                            <ButtonItem label="Mijn kenteken" onPress={() => {
                                navigation.navigate('CarView')
                            }}/>
                            <ButtonItem label="Over deze app" onPress={() => {
                                navigation.navigate('AboutView')
                            }}/>
                        </ButtonCategory>

                        <ButtonCategory>
                            <ButtonItem label="Uitloggen" onPress={logoutButtonPressed} modifier="red" arrow={false}/>
                        </ButtonCategory>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        ) : <View/>
    }

}


function mapStateToProps(state) {
    return {
        user: state.user.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        logoutButtonPressed: () => dispatch(logoutButtonPressed()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyDataView);