import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AsyncStorage, Dimensions, RefreshControl, ActivityIndicator, View, ScrollView, Text, Alert, Image, Button, Platform} from 'react-native';
import globalStyles from '../elements/GlobalStyles';
import Carousel from 'react-native-snap-carousel';

import {getPermits} from '../actions/PermitActions';
import Licence from '../elements/Licence';
import UserBlock from '../elements/UserBlock';
import Header from '../components/Header';

import FontAwesome, {Icons} from 'react-native-fontawesome';

/*import PermitCounter from '../elements/PermitCounter';*/

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');
function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideWidth = wp(75);

const itemHorizontalMargin = wp(2);
const itemWidth = slideWidth + itemHorizontalMargin * 2;


const styles = {
    text: {
        marginTop: 15,
        alignSelf: 'center'
    },
    textTime: {
        fontWeight: 'bold',
        fontSize: 22,
        lineHeight: 34,
        color: 'black'
    },
    timeContainer: {
        backgroundColor: '#e6ebf5',
        alignItems: 'center',
        alignSelf: 'stretch',
        padding: 10,
        marginTop: 20,
        marginBottom: 20,
        marginLeft: -10,
        marginRight: -10
    },
    timeContainerTop: {
        fontSize: 13,
        lineHeight: 20
    },
    slide: {
        width: itemWidth,
        marginTop: 30,
        backgroundColor: 'white',
        marginLeft: 0,
        marginRight: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        marginBottom: 20,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        marginLeft: -10,
        marginRight: -10,
        alignSelf: 'stretch'
    }
};

class PermitOverview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            permits: this.props.permits
        };

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            permits: nextProps.permits,
            refreshing: false
        });
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.props.getPermits();
    }

    render() {
        let permitList = null;
        if(this.state.permits) {
            permitList = this.state.permits.map((permit) => {
                let activePermit = permit.active_permit.started && !permit.active_permit.ended,
                    userInfo = activePermit ? (<UserBlock user={permit.active_car.user}/>) : null,
                    licence = activePermit ? (<Licence plate={permit.active_car.licence}/>) : null,
                    time, fullDate;

                if(activePermit) {
                    let date = new Date(permit.active_permit.started * 1000),
                        minutes = (`0${date.getMinutes()}`).slice(-2);

                    time = `${date.getHours()}:${minutes}`;
                    fullDate = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
                }


                let timeBlock =
                    activePermit ?
                        (<View style={styles.timeContainer}>
                            <Text style={styles.timeContainerTop}>Deze vergunning is geactiveerd om</Text>
                            <Text style={styles.textTime}>{time}</Text>
                            <Text>{fullDate}</Text>
                        </View>) :
                        (<View style={styles.timeContainer}>
                            <Text style={styles.timeContainerTop}>Deze vergunning is niet in gebruik</Text>
                        </View>);

                return (
                    <View key={permit.uuid} style={[globalStyles.mainContent, styles.slide]}>
                        <View style={[globalStyles.mainContentHeader]}>
                            <Text style={globalStyles.mainContentHeaderTitle}>Vergunning</Text>
                            <Text style={globalStyles.mainContentHeaderSubtitle}>{permit.name}</Text>
                            {licence}
                        </View>

                        {userInfo}
                        {timeBlock}

                    </View>
                );
            });
        }

        return (
            <ScrollView scrollEnabled={false} style={globalStyles.mainView}>
                <Header title="Overzicht"
                        refresher={!this.state.refreshing ? this._onRefresh.bind(this) : 'refreshing'}
                />
                <Carousel
                    showsHorizontalScrollIndicator={false}
                    ref={(carousel) => {
                        this._carousel = carousel;
                    }}
                    sliderWidth={viewportWidth}
                    itemWidth={itemWidth}>

                    {permitList}

                </Carousel>
            </ScrollView>
        );

    }
}


function mapStateToProps(state) {
    return {
        isLoading: state.permit.isLoading,
        permits: state.permit.permits,
        user: state.user.user || state.login.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getPermits: () => dispatch(getPermits())
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PermitOverview);