import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';

import {addNewCar, updateCar} from '../actions/UserActions';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import {Category, Item} from "../elements/Settings";
import globalStyles from '../elements/GlobalStyles';
import Licence from '../elements/Licence';
import SolidButton from '../elements/SolidButton';

const styles = {
    container: {
        padding: 0,
        marginTop: 15,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
};

class CarView extends Component{
    constructor(props) {
        super(props);

        let hasCar = !!this.props.user.cars.data.length;

        let plate = hasCar ? this.props.user.cars.data[0].licence : '';
        let make = hasCar ? this.props.user.cars.data[0].make : '';
        let model = hasCar ? this.props.user.cars.data[0].model : '';

        this.state = {
            userHasCar: hasCar,
            plate: plate,
            make: make,
            model: model
        };
    }

    componentWillReceiveProps(nextProps) {
        let car = nextProps.user.cars.data[0];
        this.setState({
            plate: car.licence,
            make: car.make,
            model: car.model
        });
    }

    _saveLicence() {
        if(this.state.plate === '') {
            Alert.alert('Fout', 'Je hebt geen kenteken ingevuld');
            return false;
        }
        let props = this.props,
            formattedPlate = '',
            letterCount = 0,
            numberCount = 0;


        // Making it fool proof
        for(let i = 0; i < this.state.plate.length; i++) {
            let cur = this.state.plate[i];
            if(i === this.state.plate.length - 1) {
                formattedPlate += cur.toUpperCase();

            }
            else {
                let next = this.state.plate[i + 1];

                if(cur !== '-' && cur !== ' ') {
                    if(cur.isNumber() && (next.isNumber() || next === '-' || next === ' ')) {
                        formattedPlate += cur;
                        numberCount++;
                        letterCount = 0;
                    }
                    if(cur.isNumber() && next.isLetter()) {
                        formattedPlate += cur + '-';
                        numberCount++;
                        letterCount = 0;
                    }
                    if(cur.isLetter() && next.isNumber()) {
                        formattedPlate += cur.toUpperCase() + '-';
                        letterCount++;
                        numberCount = 0;
                    }
                    if(cur.isLetter() && (next.isLetter() || next === '-' || next === ' ')) {
                        formattedPlate += cur.toUpperCase();
                        letterCount++;
                        numberCount = 0;
                    }

                    if(numberCount === 2) {
                        if(next.isNumber() && this.state.plate[i + 2].isNumber()) {
                            formattedPlate += '-';
                        }
                    }

                    if(letterCount === 2) {
                        if(next.isLetter() && this.state.plate[i + 2].isLetter()) {
                            formattedPlate += '-';
                        }
                    }
                }
                else if(cur === ' ') {
                    formattedPlate += '-';
                }
                else {
                    formattedPlate += cur;
                }
            }
        }

        if(!this.state.userHasCar) {
            props.addNewCar(formattedPlate, props.user.uuid);
        }
        else {
            props.updateCar(formattedPlate, props.user.uuid, props.user.cars.data[0].uuid);
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <KeyboardAwareScrollView
                    getTextInputRefs={() => {
                        return [this._textInputRef];
                    }}
                    style={globalStyles.mainView}
                >

                    <View style={globalStyles.mainContent}>
                        <Category title="MIJN AUTO">
                            <Item label="Merk">{this.state.make}</Item>
                            <Item label="Model">{this.state.model}</Item>
                        </Category>


                        <Category title="MIJN KENTEKEN" style={{marginTop:40}}>
                            <View style={styles.container}>
                                <Licence
                                    plate={this.state.plate}
                                    edit={true}
                                    onChange={(plate) => {
                                        this.setState({plate: plate})
                                    }}
                                    reference={(r) => {
                                        this._textInputRef = r;
                                    }}/>
                            </View>

                            <View style={styles.container}>
                                <SolidButton
                                    title="Opslaan"
                                    onPress={this._saveLicence.bind(this)}
                                />
                            </View>
                        </Category>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}


function mapStateToProps(state) {
    return {
        user: state.user.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addNewCar: (licence, userId) => dispatch(addNewCar(licence, userId)),
        updateCar: (licence, userId, carId) => dispatch(updateCar(licence, userId, carId)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CarView);