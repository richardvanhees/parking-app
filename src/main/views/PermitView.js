import React, {Component} from 'react';
import {connect} from 'react-redux';
import {register} from '../config';
import {ActivityIndicator, View, ScrollView, Text, Alert} from 'react-native';
import PopupDialog, {SlideAnimation} from 'react-native-popup-dialog';
import globalStyles from '../elements/GlobalStyles';
import {TabViewAnimated, TabBar, SceneMap} from 'react-native-tab-view';

import {getPermits, activatePermit, deactivatePermit} from '../actions/PermitActions';
import Header from '../components/Header';
import SolidButton from '../elements/SolidButton';
import OutlineButton from '../elements/OutlineButton';
import Licence from '../elements/Licence';
import UserBlock from '../elements/UserBlock';

import FontAwesome, {Icons} from 'react-native-fontawesome';

/*import PermitCounter from '../elements/PermitCounter';*/


const styles = {
    button: {
        marginBottom: 10
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        marginTop: 15,
        alignSelf: 'center'
    },
    textTime: {
        fontWeight: 'bold',
        fontSize: 22,
        lineHeight: 34,
        color: 'black'
    },
    timeContainer: {
        flex: 1,
        backgroundColor: '#e6ebf5',
        alignItems: 'center',
        alignSelf: 'stretch',
        padding: 10,
        marginTop: 20,
        marginBottom: 20,
        marginLeft: -10,
        marginRight: -10
    },
    licenceContainer: {
        marginTop: 5,
        marginBottom: 5
    },
    timeContainerTop: {
        fontSize: 13,
        lineHeight: 20
    },
    warning: {
        marginBottom: 20
    }
};

class PermitView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            permits: [],
            buttonText: '',
            buttonColor: '',
            startTime: '',
            startDate: '',
            plate: '',
            buttonType: '',
            permitNumber: '',
            permitUser: null,
            activePermit: false,
            permitsLeft: false,
            processing: false
        };
    }

    componentWillReceiveProps(nextProps) {
        let time = '', fullDate = '', activePermit = false, plate = '', permitNumber = '';

        if(nextProps.user) {
            if(nextProps.user.active_permit) {
                let date = new Date(nextProps.user.active.data[0].started * 1000);
                let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
                time = `${date.getHours()}:${minutes}`;
                fullDate = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;

                activePermit = true;

                permitNumber = nextProps.user.active.data[0].permit.name;
            }
            if(nextProps.user.cars) {
                if(nextProps.user.cars.data.length) {
                    plate = nextProps.user.cars.data[0].licence;
                }
            }
        }

        let leftPermits = 0, permitText;
        if(this.props.permits) {
            for(let permit of this.props.permits) {
                if(permit.active === false) {
                    leftPermits++;
                }
            }
        }

        switch(leftPermits) {
            case 0:
                permitText = 'Er zijn geen vergunningen meer beschikbaar';
                break;
            case 1:
                permitText = 'Er is nog 1 vergunning beschikbaar';
                break;
            default:
                permitText = `Er zijn nog ${leftPermits} vergunningen beschikbaar`;
        }


        this.setState({
            activePermit: activePermit,
            permitsLeft: !!leftPermits,
            permits: nextProps.permits,
            processing: nextProps.processing,
            refreshing: false,
            buttonText: activePermit ? 'Ik ga weer weg' : (!leftPermits ? 'Te laat!' : 'Ik wil een vergunning'),
            buttonColor: activePermit ? '#f5eb69' : '#1eb9dd',
            startTime: activePermit ? time : '',
            startDate: activePermit ? fullDate : '',
            buttonType: activePermit ? OutlineButton : SolidButton,
            permitText: permitText,
            plate: plate,
            permitNumber: activePermit ? 'Vergunning ' + permitNumber : '',
            contentHeader: activePermit ? 'Mijn vergunning' : 'Je hebt nog geen vergunning',
            permitUser: activePermit ? UserBlock : null
        });
    }

    _activateButton() {

        this.props.activatePermit(this.props.user.cars.data[0].uuid, this.props.user.uuid);
        this.activatePopup.dismiss();
    }

    _deactivateButton() {
        this.props.deactivatePermit(this.props.user.cars.data[0].uuid, this.props.user.uuid);
        this.deactivatePopup.dismiss();
    }

    _onButtonPress() {
        let props = this.props;
        if(!props.user.cars.data.length) {
            Alert.alert('Je hebt nog geen kenteken ingesteld');
            return false;
        }

        if(props.user.active_permit) {
            this.deactivatePopup.show();
        }
        else {
            this.activatePopup.show();
        }
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.props.getPermits();
    }

    render() {
        let {buttonText, permitText, buttonColor, permitNumber, contentHeader} = this.state;

        let permitViewToDisplay = (<View />);
        let PermitButton = this.state.buttonType || SolidButton;
        let UserInfo = this.state.permitUser || View;

        let PermitAction = this.state.processing ?
            (<ActivityIndicator/>) :
            (<PermitButton
                onPress={() => {
                    this._onButtonPress()
                }}
                disabled={!this.state.permitsLeft && !this.state.activePermit}
                title={buttonText}
                color={buttonColor}
                style={styles.button}/>);

        let timeBlock =
            this.state.activePermit ?
                (<View style={styles.timeContainer}>
                    <Text style={styles.timeContainerTop}>Je hebt je aangemeld om</Text>
                    <Text style={styles.textTime}>{this.state.startTime}</Text>
                    <Text>{this.state.startDate}</Text>
                </View>) :
                (<View style={styles.timeContainer}>
                    <Text style={styles.timeContainerTop}>{permitText}</Text>
                </View>);

        let warning =
            !register ?
                (<View>
                    <Text style={[styles.warning, globalStyles.warning]}>Registratie bij gemeente is uitgeschakeld!</Text>
                </View>) :
                (<View/>);

        if(!this.props.isLoading) {
            permitViewToDisplay = (
                <View style={{flex: 1}}>
                    <ScrollView
                        style={globalStyles.mainView}
                        >


                        {/*Normal view*/}
                        <View style={[styles.container, globalStyles.mainContent]}>
                            <View style={globalStyles.mainContentHeader}>
                                <Text style={globalStyles.mainContentHeaderTitle}>{contentHeader}</Text>
                                <Text style={globalStyles.mainContentHeaderSubtitle}>{permitNumber}</Text>
                                <Licence plate={this.state.plate}/>
                            </View>

                            <UserInfo user={this.props.user}/>

                            {timeBlock}
                            {warning}

                            {PermitAction}
                        </View>
                    </ScrollView>

                    {/* ========================================================== */}
                    {/*Activate Popup*/}
                    <PopupDialog
                        ref={(popupDialog) => {
                            this.activatePopup = popupDialog;
                        }}
                        animationDuration={300}
                        width={250}
                        height={360}
                        dialogAnimation={ new SlideAnimation({slideFrom: 'top'}) }
                    >


                        <ScrollView style={globalStyles.popup} alwaysBounceVertical={false}>
                            <View style={globalStyles.popupHeader}>
                                <Text style={globalStyles.popupHeaderText}>
                                    Vergunning activeren
                                </Text>
                            </View>
                            <View style={globalStyles.popupContent}>
                                <Text style={globalStyles.popupContentText}>
                                    Je staat op het punt om een auto met het volgende kenteken aan te melden:
                                </Text>
                                <View style={styles.licenceContainer}>
                                    <Licence plate={this.state.plate}/>
                                </View>
                                <Text style={globalStyles.popupContentText}>
                                    Weet je zeker dat je dit kenteken wilt aanmelden?
                                </Text>
                            </View>
                            <View style={globalStyles.popupFooter}>
                                <SolidButton
                                    onPress={this._activateButton.bind(this)}
                                    title="Aanmelden!"
                                />
                                <OutlineButton
                                    onPress={() => this.activatePopup.dismiss()}
                                    title="Toch niet"
                                />
                            </View>
                        </ScrollView>
                    </PopupDialog>


                    {/*Deactivate Popup*/}
                    <PopupDialog
                        ref={(popupDialog) => {
                            this.deactivatePopup = popupDialog;
                        }}
                        animationDuration={300}
                        height={250}
                        width={250}
                        dialogAnimation={ new SlideAnimation({slideFrom: 'top'}) }
                    >
                        <View style={globalStyles.popup}>
                            <View style={globalStyles.popupHeader}>
                                <Text style={globalStyles.popupHeaderText}>
                                    Vergunning activeren
                                </Text>
                            </View>
                            <View style={globalStyles.popupContent}>
                                <Text style={globalStyles.popupContentText}>
                                    Je staat op het punt om je kenteken weer af te melden.
                                </Text>
                                <Text style={globalStyles.popupContentText}>
                                    Weet je het zeker?
                                </Text>
                            </View>
                            <View style={globalStyles.popupFooter}>

                                <SolidButton
                                    onPress={this._deactivateButton.bind(this)}
                                    title="Afmelden!"
                                />
                                <OutlineButton
                                    onPress={() => this.deactivatePopup.dismiss()}
                                    title="Ik blijf nog"
                                />
                            </View>
                        </View>
                    </PopupDialog>


                    {/* ========================================================== */}
                    {/*Permit counter*/}
                    {/*<PermitCounter/>*/}
                </View>
            )
        } else {
            permitViewToDisplay = (
                <View style={{flex: 1}}>
                    <ActivityIndicator
                        animating={true}
                        style={[{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: 8,
                        }, {height: 80}]}
                        size="large"
                    />
                </View>
            )
        }

        return (
            <View style={{flex: 1}}>
                <Header title="Aan-/afmelden"
                        refresher={!this.state.refreshing ? this._onRefresh.bind(this) : 'refreshing'}
                />
                {permitViewToDisplay}
            </View>);

    }
}


function mapStateToProps(state) {
    return {
        isLoading: state.permit.isLoading,
        processing: state.permit.processing,
        permits: state.permit.permits,
        user: state.user.user || state.login.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getPermits: () => dispatch(getPermits()),
        activatePermit: (carId, userId) => dispatch(activatePermit(carId, userId)),
        deactivatePermit: (carId, userId) => dispatch(deactivatePermit(carId, userId))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PermitView);