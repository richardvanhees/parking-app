import {Platform, Alert} from 'react-native';
import {getPermits} from './PermitActions';
import {get, post, setHeaders} from '../utils/AxiosWrapper';
import {LoginType, UserType} from './ActionTypes';
import {storeItem} from '../utils/StorageWrapper';

const domain = Platform.OS === 'ios' ? 'localhost' : '10.0.2.2';

export function loginButtonPressed(data) {
    return (dispatch, getState) => {
        post({
            route: '/auth/login',
            data: {
                email: data.email,
                password: data.password
            },
            success: (response) => {
                storeItem('token', response.access_token);
                setHeaders(response);
                console.log(response);

                dispatch(loginSuccess(response.user));
            },
            fail: (response) => {
                dispatch(loginFailed(response))
            }
        });

    };
}

export function logoutButtonPressed() {
    return {type: LoginType.LOGOUT_BUTTON_PRESSED};
}

export function loginSuccess(response) {
    return (dispatch, getState) => {
        dispatch({type: LoginType.LOGIN_SUCCESS, response: response});
        dispatch({type: UserType.GET_USER_DATA_SUCCESS, response: response});
        dispatch(getPermits());
    }
}

export function loginFailed(responseData) {
    console.warn(responseData);
    return (dispatch, getState) => {
        Alert.alert('Inloggen is mislukt.');
        dispatch({type: LoginType.LOGIN_FAILED});
    }

}

export function setUserToken(token) {
    return (dispatch, getState) => {
        dispatch({type: LoginType.SET_TOKEN, token: token});
    }
}

export function silentLoginActivated(token) {
    setHeaders({access_token: token});

    return (dispatch, getState) => {
        get({
            route: '/auth/whoami',
            success: (response) => {
                dispatch(loginSuccess(response))
            },
            fail: (response) => {
                dispatch(loginFailed(response))
            }
        });

    };
}


