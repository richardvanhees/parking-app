import { Platform } from 'react-native';
import { get, post } from '../utils/AxiosWrapper';
import { register } from '../config';
import { getUserData } from './UserActions';
import { PermitType } from './ActionTypes';

export function getPermits() {
    return (dispatch, getState) => {
        get({
            route: '/permits',
            success: (response) => {
                dispatch(getPermitsSuccess(response));
                dispatch(getUserData());
            },
            fail: (response) =>{
                console.error(response);
            }
        });
    }
}

export function activatePermit(carId, userId){
    return (dispatch, getState) => {
        dispatch({type: PermitType.START_PERMIT_PROCESS});
        post({
            route: `/users/${userId}/claim`,
            data: {
                'car_uuid': carId,
                'register': register
            },
            success: (response) => {
                dispatch(getPermits());
            },
            fail: (response) =>{
                console.error(response);
            },
            done: () =>{
                dispatch({type: PermitType.STOP_PERMIT_PROCESS});
            }
        });
    }
}

export function deactivatePermit(carId, userId){
    return (dispatch, getState) => {
        dispatch({type: PermitType.START_PERMIT_PROCESS});
        post({
            route: `/users/${userId}/leave`,
            data: {
                'car_uuid': carId,
                'register': register
            },
            success: (response) => {
                dispatch(getPermits());
            },
            fail: (response) =>{
                console.error(response);
            },
            done: () =>{
                dispatch({type: PermitType.STOP_PERMIT_PROCESS});
            }
        });
    }
}

export function getPermitsSuccess(response) {
    return (dispatch, getState) => {
        dispatch({type: PermitType.GET_PERMITS_SUCCESS, response: response});
    }
}



