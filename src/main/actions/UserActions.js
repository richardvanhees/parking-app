import {get, post, getExternal} from '../utils/AxiosWrapper';
import {Platform, Alert} from 'react-native';
import {UserType} from './ActionTypes';
import { getHistory } from './HistoryActions';

export function getUserData() {
    return (dispatch, getState) => {
        get({
            route: '/auth/whoami',
            success: (response) => {
                response.history = {};
                dispatch(getUserDataSuccess(response));
                dispatch(getHistory(response.uuid));


                // For users who don't have this info yet
                if(response.cars.data.length){
                    let car = response.cars.data[0];
                    if(!car.make){
                        dispatch(updateCar(car.licence, response.uuid, car.uuid, true));
                    }
                }
            },
            fail: (response) => {
                console.log(response);
                console.error('Unable to get user data');
            }
        });
    }
}

export function getUserDataSuccess(response) {
    return (dispatch, getState) => {
        dispatch({type: UserType.GET_USER_DATA_SUCCESS, response: response});
    }
}

export function addNewCar(licence, userId) {
    return (dispatch, getState) => {
        post({
            route: `/users/${userId}/cars`,
            data: {licence: licence},
            success: (response) => {
                Alert.alert('Het nummerbord is succesvol toegevoegd');
                dispatch({type: UserType.ADDED_NEW_CAR});
                dispatch(getUserData());
                dispatch(getCarDetails(licence));
            },
            fail: (response) => {
                console.error(response);
            }
        })
    };
}

export function updateCar(licence, userId, carId, noAlert) {
    return (dispatch, getState) => {
        getCarDetails(licence, (response) => {
            response.merk = response.merk.ucfirst();
            response.handelsbenaming = response.handelsbenaming.ucfirst();
            response.eerste_kleur = response.eerste_kleur.ucfirst();

            post({
                route: `/users/${userId}/cars/${carId}`,
                data: {
                    licence: licence,
                    make: response.merk || null,
                    model: response.handelsbenaming || null,
                    color: response.eerste_kleur || null
                },
                success: () => {
                    if(!noAlert){
                        Alert.alert('Het nummerbord is succesvol gewijzigd');
                    }
                    dispatch({type: UserType.UPDATED_CAR});
                    dispatch(getUserData());
                },
                fail: (response) => {
                    console.error(response);
                }
            });
        });
    }
}

export function getCarDetails(licence, callback) {

    console.log(licence);
    getExternal(
        'https://opendata.rdw.nl',
        {
            route: `/resource/m9d7-ebf2.json?kenteken=${licence.replace(/-/g, '').toUpperCase()}`,
            success: (response) => {
                if(callback) callback(response[0]);
            },
            fail: (response) => {
                console.log(response);
            }
        });

}



