import { Platform } from 'react-native';
import { get } from '../utils/AxiosWrapper';
import { HistoryType } from './ActionTypes';

export function getHistory(userId) {
    return (dispatch, getState) => {
        get({
            route: `/users/${userId}/history`,
            success: (response) => {
                dispatch(getHistorySuccess(response.data));
            },
            fail: (response) =>{
                console.error(response);
            }
        });
    }
}

export function getHistorySuccess(data) {
    console.log('historyresponse:', data);
    return (dispatch, getState) => {
        dispatch({type: HistoryType.GET_HISTORY_SUCCESS, response: data});
    }
}





