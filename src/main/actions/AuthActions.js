import {AuthTypes} from './ActionTypes';

function authSetToken(token) {
    return {
        type: AuthTypes.AUTH_SET_TOKEN,
        token
    };
}

function authDiscardToken() {
    return {
        type: AuthTypes.AUTH_DISCARD_TOKEN
    };
}

function authSetUser(user) {
    return {
        type: AuthTypes.AUTH_SET_USER,
        user
    };
}

module.exports = {
    authSetToken,
    authDiscardToken,
    authSetUser
};