import {NavigationType} from './ActionTypes';
import {getPermits} from './PermitActions';

export function permitViewButtonPressed() {
    return (dispatch, getState) => {
        dispatch({type: NavigationType.PERMITVIEW_BUTTON_PRESSED});
        dispatch(getPermits());
    }
}

export function myDataViewButtonPressed() {
    return {type: NavigationType.MY_DATAVIEW_BUTTON_PRESSED}
}

export function permitOverviewButtonPressed() {
    return {type: NavigationType.PERMIT_OVERVIEW_BUTTON_PRESSED};
}

export function aboutViewButtonPressed() {
    return {type: NavigationType.ABOUT_BUTTON_VIEW_PRESSED};
}

export function historyViewButtonPressed() {
    return {type: NavigationType.HISTORY_BUTTON_VIEW_PRESSED};
}