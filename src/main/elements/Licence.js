import React, {Component} from 'react';
import {View, Text, TextInput, Platform} from 'react-native';

let fontSizeLarge = 24;
let fontSizeSmall = 16;

let styles = {
    container: {
        borderColor: 'black',
        backgroundColor: '#efd102',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
    },
    container__large: {
        alignSelf: 'center',
        height: 40,
        width: 160,
        borderWidth: 3,
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5,
    },
    container__small: {
        height: 26,
        width: 100,
        borderWidth: 2,
        borderRadius: 3,
    },


    number: {
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
    },
    number__large: {
        fontSize: fontSizeLarge,
        /*height: Platform.select({
            ios: 34,
            android: 34
        })*/
    },
    number__small: {
        fontSize: fontSizeSmall,
    },

    input: {
        // For some reason the boldness depends on these three props. It doesn't work with hot reloading; needs full reload for bold rendering.
        padding: 0,
        height: fontSizeLarge - 4,
        lineHeight: fontSizeLarge,
        width: 154
    }
};

class Licence extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: this.props.edit || false
        };
    }

    render() {
        let size = this.props.inline ? 'small' : 'large';
        let TextBlock = this.state.edit ?
            (<TextInput
                underlineColorAndroid="transparent"
                style={[styles.number, styles['number__' + size], styles.input]}
                defaultValue={this.props.plate}
                placeholder="Vul in..."
                onChangeText={this.props.onChange}
                ref={this.props.reference}
            />)
            : (<Text style={[styles.number, styles['number__' + size]]}>
                {this.props.plate}
            </Text>);


        return (
            <View style={[styles.container, styles['container__' + size]]}>
                {TextBlock}
            </View>
        );
    }
}

export default Licence;