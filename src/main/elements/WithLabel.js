import React, {Component} from 'react';
import {View, Text} from 'react-native';

var styles = {
    labelContainer: {
        flexDirection: 'row',
        marginVertical: 2,
        flex: 1
    },
    label: {
        height: 43,
        width: 115,
        alignItems: 'flex-end',
        marginRight: 10
    },
    labelText:{
        fontSize: 16,
        lineHeight: 43
    },
    default: {
        height: 45,
        borderColor: '#c8c7cc',
        flex: 1,
        borderBottomWidth: 1
    }
}

class WithLabel extends React.Component {
    render() {
        return (
            <View style={styles.labelContainer}>
                <View style={styles.label}>
                    <Text style={styles.labelText}>{this.props.label}</Text>
                </View>
                <View style={styles.default}>
                    {this.props.children}
                </View>
            </View>
        );
    }
}

export default WithLabel;