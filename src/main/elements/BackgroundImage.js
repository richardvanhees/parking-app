import React, {Component} from 'react';
import {Image} from 'react-native';

let styles = {
    backgroundImage: {
        width: null,
        height: null,
        flex: 1,
        resizeMode: 'cover'
    }
}

class BackgroundImage extends React.Component {
    shouldComponentUpdate(nextProps, nextState){
        return false;
    }

    props: {
        source: string
    };


    render() {
        const {
            children
        } = this.props;


        return (
            <Image source={require('../static/images/gevel.png')} style={styles.backgroundImage}>
                {children}
            </Image>
        );
    }
}

export default BackgroundImage;