import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';

import {Icons} from 'react-native-fontawesome';

const styles = StyleSheet.create({
    icon: {
        fontFamily: 'FontAwesome',
        backgroundColor: 'transparent',
        fontWeight: 'normal'
    },
});

export default class Icon extends Component {
    render() {
        return (
            <Text style={[styles.icon]}>
                {Icons[this.props.name]}
            </Text>
        );
    }
}
