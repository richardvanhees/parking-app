import React, {Component} from 'react';
import {View, Text, Dimensions, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';

let {height, width} = Dimensions.get('window');

export function startLoader() {

}

export function stopLoader() {

}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'black',
        position: 'absolute',
        opacity: 0.4,
        height: height,
        width: width + 3
    },
    text:{

    }
};


class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bla: false
        }
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
                <Text>Ogenblikje...</Text>
            </View>
        );
    }
}

export default Loader;