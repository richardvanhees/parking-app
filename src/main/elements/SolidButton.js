'use strict';

const ColorPropType = require('ColorPropType');
const Platform = require('Platform');
const React = require('React');
const StyleSheet = require('StyleSheet');
const Text = require('Text');
const TouchableNativeFeedback = require('TouchableNativeFeedback');
const TouchableOpacity = require('TouchableOpacity');
const View = require('View');

const invariant = require('fbjs/lib/invariant');

let defaultColor = '#1d6b34';

const styles = StyleSheet.create({
    button: {
        elevation: 2,
        backgroundColor: defaultColor,
        borderRadius: 3,
        borderWidth: 3,
        borderColor: defaultColor,
        margin: 5
    },
    text: {
        textAlign: 'center',
        color: 'white',
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 15,
        paddingRight: 15,
        fontWeight: '500',
    },
    buttonDisabled: {
        elevation: 0,
        backgroundColor: '#dfdfdf',
        borderColor: '#dfdfdf'
    },
    textDisabled: {
        color: '#a1a1a1',
    },
});

class SolidButton extends React.Component {

    props: {
        title: string,
        onPress: () => any,
        accessibilityLabel?: ?string,
        disabled?: ?boolean
    };

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        accessibilityLabel: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        onPress: React.PropTypes.func.isRequired,
    };

    render() {
        const {
            accessibilityLabel,
            onPress,
            title,
            disabled,
            style
        } = this.props;
        const buttonStyles = [styles.button];
        const textStyles = [styles.text];
        const Touchable = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;



        if(disabled) {
            buttonStyles.push(styles.buttonDisabled);
            textStyles.push(styles.textDisabled);
        }
        if(style){
            buttonStyles.push(style);
        }

        invariant(
            typeof title === 'string',
            'The title prop of a SolidButton must be a string',
        );
        return (
            <Touchable
                accessibilityComponentType="button"
                accessibilityLabel={accessibilityLabel}
                accessibilityTraits={['button']}
                disabled={disabled}
                onPress={onPress}>
                <View style={buttonStyles}>
                    <Text style={textStyles}>{title}</Text>
                </View>
            </Touchable>
        );
    }
}

module.exports = SolidButton;
