'use strict';

const ColorPropType = require('ColorPropType');
const Platform = require('Platform');
const React = require('React');
const StyleSheet = require('StyleSheet');
const Text = require('Text');
const TouchableNativeFeedback = require('TouchableNativeFeedback');
const TouchableOpacity = require('TouchableOpacity');
const View = require('View');

const invariant = require('fbjs/lib/invariant');

let defaultColor = '#1d6b34';

const styles = StyleSheet.create({
    button: {
        elevation: 0,
        borderColor: defaultColor,
        borderWidth: 2,
        borderRadius: 3,
        margin: 5
    },
    text: {
        textAlign: 'center',
        color: defaultColor,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        fontWeight: '500',
    },
    buttonDisabled: {
        elevation: 0,
        backgroundColor: '#dfdfdf',
    },
    textDisabled: {
        color: '#a1a1a1',
    },
});

class OutlineButton extends React.Component {

    props: {
        title: string,
        onPress: () => any,
        accessibilityLabel?: ?string,
        disabled?: ?boolean,
        style?: any
    };

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        accessibilityLabel: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        onPress: React.PropTypes.func.isRequired,
    };

    render() {
        const {
            accessibilityLabel,
            onPress,
            title,
            disabled,
            style
        } = this.props;
        const buttonStyles = [styles.button];
        const textStyles = [styles.text];
        const Touchable = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

        if(disabled) {
            buttonStyles.push(styles.buttonDisabled);
            textStyles.push(styles.textDisabled);
        }
        invariant(
            typeof title === 'string',
            'The title prop of a OutlineButton must be a string',
        );
        return (
            <Touchable
                accessibilityComponentType="button"
                accessibilityLabel={accessibilityLabel}
                accessibilityTraits={['button']}
                disabled={disabled}
                onPress={onPress}>
                <View style={buttonStyles}>
                    <Text style={textStyles}>{title}</Text>
                </View>
            </Touchable>
        );
    }
}

module.exports = OutlineButton;
