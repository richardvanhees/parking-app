import {StyleSheet, Platform} from 'react-native';

export const primaryColor = '#1d6b34';


export default {
    header: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: 80
    },
    stackHeader:{
        backgroundColor: '#fff',
        height: 70,
        marginTop: -20,
    },
    textPrimaryColor:{
        color: primaryColor
    },
    mainView: {
        flex: 1,
        backgroundColor: '#e6ebf3',
    },
    mainContent: {
        paddingTop: 20,
        paddingBottom: 30,
        paddingLeft: 10,
        paddingRight: 10,
        margin: 25,
        borderRadius: 5,
        borderColor: '#c3c8cf',
        backgroundColor: 'white',
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.07,
        borderWidth: Platform.select({
            ios: 0,
            android: 1
        })
    },
    mainContentHeader: {
        marginBottom: 20,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        marginLeft: -10,
        marginRight: -10,
        alignSelf: 'stretch'
    },
    mainContentHeaderTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'black'
    },
    mainContentHeaderSubtitle: {
        alignSelf: 'center',
        color: '#8e8e8e',
        marginTop: 5,
        marginBottom: 5
    },
    subHeader: {
        alignSelf: 'stretch',
        //backgroundColor: '#e6ebf3',
        borderBottomWidth: 1,
        borderBottomColor: '#e6ebf3',
        paddingLeft: 10,
        marginLeft: -10,
        marginRight: -10,
        marginTop: 10,
        height: 20

    },
    subHeaderText:{
        fontSize: 13
    },
    titleHeader: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6'
    },
    title: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 22,
        flex: 1,
    },
    logo: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoImg: {},
    logoCaption: {
        paddingLeft: 14,
    },
    logoCaptionText: {
        fontSize: 24,
        lineHeight: 30,
        padding: 0,
        fontWeight: 'bold',
        fontFamily: 'Arial',
        color: primaryColor
    },
    titleText: {
        fontSize: 20,
        lineHeight: 20,
        padding: 0,
        fontWeight: 'bold',
        color: primaryColor
    },
    inputLabel: {
        fontWeight: 'bold',
        marginLeft: 20,
        marginRight: 20,
        fontSize: 16,
        lineHeight: 16,
        color: '#000',
        marginTop: 0,
        marginBottom: 8,
        fontFamily: 'Arial',
    },
    inputContainer: {
        borderWidth: 1,
        borderColor: '#DEDEDE',
        borderRadius: 0,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
    },
    input: {
        borderColor: 'transparent',
        height: 33,
        fontSize: 14,
        lineHeight: 30,
        padding: 0,
        paddingLeft: 10,
    },
    image: {},
    button: {
        padding: 14,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 4,
        height: null,
        width: null,
        overflow: 'hidden',
    },
    buttonBlock: {
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#1eb9dd',
        alignItems: 'stretch',
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        backgroundColor: 'transparent',
    },
    buttonContainer: {
        alignItems: 'stretch',
        paddingTop: 10,
        paddingBottom: 25,
        marginLeft: 20,
        marginRight: 20,
    },
    heading: {
        color: '#000',
        fontSize: 21,
        lineHeight: 29,
        fontWeight: 'bold',
    },
    p: {
        fontSize: 14,
        lineHeight: 22,
        color: '#000',
        marginBottom: 22,
    },
    strong: {
        fontWeight: 'bold',
    },
    popup: {
        padding: 20,
    },
    popupHeader: {
        alignItems: 'center'
    },
    popupHeaderText: {
        fontWeight: 'bold'
    },
    popupContent: {
        paddingTop: 20,
    },
    popupContentText: {
        textAlign: 'center'
    },
    popupFooter: {
        paddingTop: 20
    },
    warning:{
        color: 'red',
        fontWeight: 'bold'
    }
};