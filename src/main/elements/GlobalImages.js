import { Platform } from 'react-native';

var images = {
    hamburger: {
        src: require('../static/images/hamburger-tmp.png'),
        size: [25, 19]
    },
    logo: {
        src: require('../static/images/denhaag.png'),
        //size: [126/3, 94/3],
        size: [150, 50],
    },
    acato:{
        src: require('../static/images/acato-logo.png'),
        size: [150, 50]
    },
    alert: {
        src: require('../static/images/alert-tmp.png'),
        size: [72/3.5, 74/3.5],
    },
    background: {
        src: require('../static/images/gevel.png'),
    },
    buttonBackground: {
        src: require('../static/images/button-bg.png'),
        size: [100, 50],
    },
    arrowLeft: {
        src: require('../static/images/back-arrow.png'),
        size: [25, 19],
    },

    progressStart: {
        src: require('../static/images/progress-start.png'),
        size: [39/3, 39/3],
    },
    progressEnd: {
        src: require('../static/images/progress-end.png'),
        size: [57/3, 57/3],
    },
    noTime: {
        src: require('../static/images/no-time.png'),
        size: [57/4, 57/4],
    },
    noTimePink: {
        src: require('../static/images/no-time-pink.png'),
        size: [57/4, 57/4],
    },

};


if(Platform.OS === 'android') {
    images.buttonBackground.src = require('../static/images/transparent.png');
}


module.exports = images;
