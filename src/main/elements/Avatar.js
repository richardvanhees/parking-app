import React, {Component} from 'react';
import {View, Text, Platform} from 'react-native';
import FontAwesome, {Icons} from 'react-native-fontawesome';

let styles = {
    circle: {
        justifyContent: 'center',
        flex: 1,
        width: 36,
        height: 38,
        borderRadius: 19,
        backgroundColor: '#f1f1f1',
        borderColor: '#f1f1f1',
        borderWidth: 1,
        overflow: 'hidden',
    },
    text: {
        color: '#8e8e8e',
        fontWeight: 'bold',
        backgroundColor: 'transparent',
        width: 30,
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 17
    }
}

class Avatar extends React.Component {
    componentWillReceiveProps(nextProps) {

        this.setState({
            letters: nextProps.letters.trim()
        });
    }

    render() {
        return (
            <View style={styles.circle}>
                <Text style={styles.text}>
                    {this.props.letters.trim()}
                </Text>
            </View>
        );
    }
}


export default Avatar;