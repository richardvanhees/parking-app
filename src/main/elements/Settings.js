import React, {Component} from 'react';
import {Text, View, TouchableHighlight} from 'react-native';

import globalStyles, {primaryColor} from './GlobalStyles';
import FontAwesome from '../elements/fontawesome/FontAwesome';

const styles = {
    category: {
        flex: 1,
        alignSelf: 'stretch'
    },
    value: {
        height: 36,
        lineHeight: 36,
        color: '#8e8e8e',
        textAlign: 'right',
        flex: 80
    },
    label: {
        height: 36,
        lineHeight: 36,
        flex: 20
    }

};

export class Category extends Component {
    render() {
        let catStyles = [styles.category];
        if(this.props.style) catStyles.push(this.props.style);

        return (
            <View style={catStyles}>
                <View style={globalStyles.subHeader}>
                    <Text style={globalStyles.subHeaderText}>{this.props.title}</Text>
                </View>
                {this.props.children}
            </View>
        );
    }
}

export class Item extends Component {

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={styles.label}>{this.props.label}</Text>
                <Text style={styles.value}>{this.props.children}</Text>
            </View>
        );
    }
}

// ===============================================================

const buttonStyles = {
    category: {
        paddingTop: 20
    },
    buttonContainer: {
        flex: 1,
        marginLeft: -10,
        marginRight: -10,
    },
    buttonInner: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#e6ebf3',
        borderTopWidth: 1,
        borderTopColor: '#e6ebf3',
        padding: 10,
        backgroundColor: '#f6f6f6',
        marginTop: -1
    },
    buttonLabel: {
        flex: 1
    },
    buttonValue: {
        flex: 1,
        alignSelf: 'flex-end',
        textAlign: 'right',
        color: '#8e8e8e'
    },
    buttonIcon: {
        color: '#8e8e8e',
        marginTop: -1.3,
        marginLeft: 15,
        fontSize: 18,
    }
};

export class ButtonCategory extends Component {
    render() {
        return (
            <View style={buttonStyles.category}>
                {this.props.children}
            </View>
        );
    }
}

export class ButtonItem extends Component {
    constructor(props) {
        super(props);
        let {arrow = true} = props;

        this.state = {
            arrow: this.props.arrow || arrow
        }
    }

    render() {
        let red = this.props.modifier === 'red';

        return (
            <TouchableHighlight onPress={this.props.onPress} style={buttonStyles.buttonContainer} underlayColor={!red ? primaryColor : '#f00'}>
                <View style={buttonStyles.buttonInner}>
                    <Text style={[buttonStyles.buttonLabel, {color: red ? '#f66' : '#000'}]}>{this.props.label}</Text>
                    <Text style={buttonStyles.buttonValue}>{this.props.children}</Text>
                    {this.state.arrow ? <Text style={buttonStyles.buttonIcon}><FontAwesome name="angleRight"/></Text> : null}
                </View>
            </TouchableHighlight>
        );
    }
}