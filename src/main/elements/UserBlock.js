import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';

import Avatar from '../elements/Avatar';
import globalStyles from '../elements/GlobalStyles';

let styles = {
    container: {
        flexDirection: 'row',
        alignSelf: 'center'
    },
    avatarContainer:{
        flex: 2,
        paddingLeft: 10
    },
    textContainer:{
        flex: 8
    },
    text: {
        fontSize: 12,
        lineHeight: 18,
        color: 'black'
    }
};

class UserBlock extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstNameLetter: this.props.user.profile.name_first[0],
            lastNameLetter: this.props.user.profile.name_last[0]
        }
    }

    componentWillReceiveProps(nextProps) {
        let firstNameLetter = '', lastNameLetter = '';
        if(nextProps.user) {
            firstNameLetter = nextProps.user.profile.name_first[0];
            lastNameLetter = nextProps.user.profile.name_last[0];
        }

        this.setState({
            firstNameLetter: firstNameLetter,
            lastNameLetter: lastNameLetter
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.avatarContainer}>
                    <Avatar letters={this.state.firstNameLetter + this.state.lastNameLetter}/>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>In gebruik door:</Text>
                    <Text style={[styles.text, globalStyles.strong]}>{this.props.user.profile.name_full}</Text>
                </View>
            </View>
        );
    }
}

export default UserBlock;