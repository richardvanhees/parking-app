import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Image, TouchableWithoutFeedback, Text, StyleSheet, ScrollView, Platform} from 'react-native';
import FontAwesome, {Icons} from 'react-native-fontawesome';
import Icomoon from '../elements/icomoon/Icomoon';

import globalStyles from '../elements/GlobalStyles';
import {logoutButtonPressed} from '../actions/LoginActions';
import {permitViewButtonPressed, myDataViewButtonPressed, aboutViewButtonPressed, permitOverviewButtonPressed, historyViewButtonPressed} from '../actions/NavigationActions';

let itemHeight = 50;

let styles = {
    item: {
        padding: 0,
        paddingLeft: 28,
        justifyContent: 'center',
        height: itemHeight,
    },
    itemText: {
        fontSize: 20,
        color: '#000',
        flex: 5,
    },
    itemIcon: {
        flex: 1,
        textAlign: 'center',
        fontSize: 14,
        paddingTop: Platform.select({
            ios: 5,
            android: 8
        })
    },
    textContainer: {
        flexDirection: 'row',
    },
    menu: {
        flex: 1,
    },
    menuMain: {
        height: 40,
        flex: 1,
        marginTop: 20,
    },
    menuBottom: {
        height: 60,
        marginBottom: 50,
    },
    activeItem: {
        backgroundColor: '#f0f3f7'
    }
};

class MenuItem extends Component {
    render() {
        let itemStyle = [styles.item];

        if(this.props.active) {
            itemStyle.push(styles.activeItem);
        }

        let IconFont = this.props.icomoon ? Icomoon : FontAwesome;


        return (
            <View style={itemStyle}>
                <View style={styles.textContainer}>
                    <Text style={[styles.itemIcon, globalStyles.textPrimaryColor]}>
                        <IconFont>{this.props.icon}</IconFont>
                    </Text>
                    <Text style={styles.itemText}>
                        {this.props.title}
                    </Text>
                </View>
            </View>
        )
    }
}

class Menu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: this.props.selectedBody
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: nextProps.selectedBody
        });
    }

    _onPressLogout() {
        this.props.logoutButtonPressed()
    }

    _onPressPermitView() {
        this.props.permitViewButtonPressed()
        this.props.closeDrawer()
    }

    _onPressMyDataView() {
        this.props.myDataViewButtonPressed()
        this.props.closeDrawer()
    }

    onPressPermitOverview() {
        this.props.permitOverviewButtonPressed();
        this.props.closeDrawer();
    }

    _onPressAboutView() {
        this.props.aboutViewButtonPressed()
        this.props.closeDrawer()
    }

    _onPressHistoryView() {
        this.props.historyViewButtonPressed()
        this.props.closeDrawer()
    }

    render() {
        return (
            <View style={styles.menu}>
                {this.props.topSpacing}
                <ScrollView style={styles.menuMain}
                            alwaysBounceVertical={false}>
                    <TouchableWithoutFeedback onPress={() => this._onPressPermitView()}>
                        <View><MenuItem title="Aan-/afmelden" icon="exchange" active={this.state.selected == 'permitView'}/></View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={() => this.onPressPermitOverview()}>
                        <View><MenuItem title="Overzicht" icon="clone" active={this.state.selected == 'permitOverview'}/></View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={() => this._onPressMyDataView()}>
                        <View><MenuItem title="Mijn account" icon="child" iconSize="20" active={this.state.selected == 'mydataView'}/></View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={() => this._onPressHistoryView()}>
                        <View><MenuItem title="Geschiedenis" icon="clockO" iconSize="18" active={this.state.selected == 'historyView'}/></View>
                    </TouchableWithoutFeedback>
                </ScrollView>

                <View style={styles.menuBottom}>
                    <TouchableWithoutFeedback onPress={() => this._onPressAboutView()}>
                        <View><MenuItem title="Over deze app" icon="acato" iconSize="16" icomoon={true} active={this.state.selected == 'aboutView'}/></View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this._onPressLogout()}>
                        <View><MenuItem title="Uitloggen" icon="powerOff" iconSize="18"/></View>
                    </TouchableWithoutFeedback>
                </View>

            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        selectedBody: state.body.selectedBody
    };
}

function mapDispatchToProps(dispatch) {
    return {
        logoutButtonPressed: () => dispatch(logoutButtonPressed()),
        permitViewButtonPressed: () => dispatch(permitViewButtonPressed()),
        myDataViewButtonPressed: () => dispatch(myDataViewButtonPressed()),
        permitOverviewButtonPressed: () => dispatch(permitOverviewButtonPressed()),
        aboutViewButtonPressed: () => dispatch(aboutViewButtonPressed()),
        historyViewButtonPressed: () => dispatch(historyViewButtonPressed()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu);