import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Image, ScrollView, StyleSheet, Text, Button, TextInput, ActivityIndicator, TouchableWithoutFeedback, KeyboardAvoidingView} from 'react-native';

import globalStyles from '../elements/GlobalStyles';
import images from '../elements/GlobalImages';
import {loginButtonPressed, silentLoginActivated} from '../actions/LoginActions';

import SolidButton from '../elements/SolidButton';
import BackgroundImage from '../elements/BackgroundImage';
import OutlineButton from '../elements/OutlineButton';

import {getItem} from '../utils/StorageWrapper';

var styles = StyleSheet.create({

    introTitle: {
        color: '#1d6b34',
        textAlign: 'center',
        lineHeight: 24,
        fontSize: 18,
        fontWeight: "bold",
        padding: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginTop: 10
    },
    input: {
        color: 'white',
        borderColor: 'white'
    },
    inputLabel: {
        paddingLeft: 20,
        paddingBottom: 5
    },
    inputLabelText: {
        fontWeight: 'bold'
    },
    container: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: 300
    },
    containerSet: {
        marginTop: 80,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'space-between',
        alignItems: 'stretch',
    },
    button: {
        marginBottom: 10,
    },
    background: {
        backgroundColor: '#1eb9dd'
    }
});

class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'richardvanhees@acato.nl',
            password: 'richard',
            isLoading: true
        }
    }

    _onLoginButtonPress() {
        this.props.loginButtonPressed(this.state);
    }


    componentWillMount() {
        getItem('token').then((token) => {
            if(token) {
                // Silent login
                this.props.silentLoginActivated(token);
            }
            else {
                this.setState({
                    isLoading: false
                });
            }
        });
    }

    render() {
        let LoginViewToDisplay = (<View />);


        if(!this.state.isLoading) {
            LoginViewToDisplay = (
                <BackgroundImage source={images.background.src}>
                    <KeyboardAvoidingView style={globalStyles.background} behavior='position'>

                        <View style={globalStyles.header}>

                            <View style={globalStyles.logo}>
                                <Image source={images.logo.src}
                                       style={globalStyles.logoImg}
                                       width={images.logo.size[0]}
                                       height={images.logo.size[1]}/>
                            </View>

                        </View>

                        <View style={styles.containerSet}>
                            <View style={styles.container}>
                                <Text style={styles.introTitle}>
                                    Inloggen
                                </Text>

                                <View style={styles.inputLabel}>
                                    <Text style={styles.inputLabelText}>E-mailadres</Text>
                                </View>
                                <View style={globalStyles.inputContainer}>
                                    <TextInput
                                        underlineColorAndroid="transparent"
                                        style={globalStyles.input}
                                        defaultValue=''
                                        onChangeText={(email) => {
                                            this.setState({email: email})
                                        }}
                                    /></View>

                                <View style={styles.inputLabel}>
                                    <Text style={styles.inputLabelText}>Wachtwoord</Text>
                                </View>
                                <View style={globalStyles.inputContainer}>
                                    <TextInput
                                        underlineColorAndroid="transparent"
                                        style={globalStyles.input}
                                        defaultValue=''
                                        secureTextEntry={true}
                                        onChangeText={(password) => {
                                            this.setState({password: password})
                                        }}
                                    /></View>


                                <View style={globalStyles.buttonContainer}>
                                    <SolidButton
                                        onPress={this._onLoginButtonPress.bind(this)}
                                        title="Inloggen"
                                        style={styles.button}/>
                                </View>
                            </View>
                        </View>

                    </KeyboardAvoidingView>
                    </BackgroundImage>

            );
        }
        else {
            LoginViewToDisplay = (
                <View style={{flex: 1}}>
                    <ActivityIndicator
                        animating={true}
                        style={[{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: 8,
                        }, {height: 80}]}
                        size="large"
                    />
                </View>
            )
        }

        return LoginViewToDisplay;
    }
}
;


function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        loginButtonPressed: (data) => dispatch(loginButtonPressed(data)),
        silentLoginActivated: (token) => dispatch(silentLoginActivated(token))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginView);
