import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Image, ScrollView, Text} from 'react-native';

import LoginView from '../components/LoginView';
import AboutView from '../components/AboutView';
import MyDataView from '../components/MyDataView';
import PermitOverview from '../components/PermitOverview';
import PermitView from '../components/PermitView';
import HistoryView from '../components/HistoryView';

class Body extends Component {
    bodyViewToDisplay(selectedBody) {
        switch(selectedBody) {
            case 'loginView':
                return <LoginView/>;
            case 'aboutView':
                return <AboutView/>;
            case 'mydataView':
                return <MyDataView/>;
            case 'permitOverview':
                return <PermitOverview/>;
            case 'permitView':
                return <PermitView/>;
            case 'historyView':
                return <HistoryView/>;
            default:
                return <PermitView/>;
        }
    }

    render() {
        let bodyViewToDisplay = this.bodyViewToDisplay(this.props.selectedBody);
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    {bodyViewToDisplay}
                </View>
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn,
        selectedBody: state.body.selectedBody
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Body);