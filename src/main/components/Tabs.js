import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, Platform, TabBarIOS, ViewPagerAndroid} from 'react-native';
import {TabViewAnimated, TabBar, SceneMap} from 'react-native-tab-view';
import Icons from '../static/images/Icons';

import globalStyles, {primaryColor} from '../elements/GlobalStyles';
import {logoutButtonPressed} from '../actions/LoginActions';
import {permitViewButtonPressed, myDataViewButtonPressed, aboutViewButtonPressed, permitOverviewButtonPressed, historyViewButtonPressed} from '../actions/NavigationActions';

import MoreView from '../views/MoreView';
import PermitOverview from '../views/PermitOverview';
import PermitView from '../views/PermitView';

let itemHeight = 50;

let styles = {
    viewPager: {
        flex: 1
    }
};

class Tabs extends Component {
    state = {
        selected: this.props.selectedBody,
        index: 0,
        routes: [
            {key: '0', title: 'Vergunning'},
            {key: '1', title: 'Overzicht'},
            {key: '2', title: 'Meer'}
        ]
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: nextProps.selectedBody
        });
    }

    _onPressHistoryView() {
        this.props.historyViewButtonPressed()
    }


    _renderScene = SceneMap({
        '0': PermitView,
        '1': PermitOverview,
        '2': MoreView,
    });


    _renderFooter(props) {
        return (<TabBar {...props} style={{backgroundColor: primaryColor}} />);
    }

    _handleChangeTab(index) {
        console.log(index);
        this.setState({
            index: index
        });
    }

    render() {
        let TabbarToDisplay = Platform.OS === 'ios' ?
            <TabBarIOS
                initialPage={0}>
                <TabBarIOS.Item
                    icon={{uri: Icons.permit, scale: 20}}
                    onPress={this.props.permitViewButtonPressed}
                    title="Vergunning"
                    selected={this.state.selected === 'permitView'}>
                    <PermitView />
                </TabBarIOS.Item>
                <TabBarIOS.Item
                    icon={{uri: Icons.overview, scale: 20}}
                    onPress={this.props.permitOverviewButtonPressed}
                    title="Overzicht"
                    selected={this.state.selected === 'permitOverview'}>
                    <PermitOverview />
                </TabBarIOS.Item>
                <TabBarIOS.Item
                    icon={{uri: Icons.user, scale: 16}}
                    onPress={this.props.myDataViewButtonPressed}
                    title="Meer"
                    selected={this.state.selected === 'mydataView'}>
                    <MoreView />
                </TabBarIOS.Item>
            </TabBarIOS> :
            <View style={{flex: 1}}>
                <TabViewAnimated
                    style={{flex: 1}}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderFooter={this._renderFooter}
                    onRequestChangeTab={this._handleChangeTab.bind(this)}
                /></View>;

        return TabbarToDisplay;
    }
}


function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn,
        selectedBody: state.body.selectedBody
    };
}

function mapDispatchToProps(dispatch) {
    return {
        logoutButtonPressed: () => dispatch(logoutButtonPressed()),
        permitViewButtonPressed: () => dispatch(permitViewButtonPressed()),
        myDataViewButtonPressed: () => dispatch(myDataViewButtonPressed()),
        permitOverviewButtonPressed: () => dispatch(permitOverviewButtonPressed()),
        aboutViewButtonPressed: () => dispatch(aboutViewButtonPressed()),
        historyViewButtonPressed: () => dispatch(historyViewButtonPressed()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tabs);