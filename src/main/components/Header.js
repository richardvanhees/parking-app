import React, {Component} from 'react';
import {View, Image, TouchableWithoutFeedback, Text, StyleSheet, ActivityIndicator, ScrollView} from 'react-native';

import globalStyles from '../elements/GlobalStyles';
import FontAwesome from '../elements/fontawesome/FontAwesome';

let styles = {
    menuButtonContainer: {
        position: "absolute",
        left: 0,
        top: 0,
        padding: 20,
    },
    refresherContainer: {
        position: "absolute",
        right: 5,
        height: 40,
        width: 40,
        alignItems: 'center'
    },
    refresherIcon: {
        paddingTop: 8
    }
};

export default class Header extends Component {
    render() {
        let refresher =
            this.props.refresher ? (
                this.props.refresher === 'refreshing' ?
                    (<ActivityIndicator style={styles.refresherContainer}/>) :
                    (<TouchableWithoutFeedback onPress={this.props.refresher}>
                        <View style={styles.refresherContainer}>
                            <Text style={[styles.refresherIcon, globalStyles.titleText]}>
                                <FontAwesome name="refresh" />
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>)) :
                (<View/>);

        return (
            <View style={globalStyles.titleHeader} elevation={0}>
                <View style={globalStyles.title}>
                    <View style={globalStyles.logoCaption}>
                        <Text style={globalStyles.titleText}>
                            {this.props.title}
                        </Text>
                    </View>
                </View>

                {refresher}

            </View>
        );
    }
};
