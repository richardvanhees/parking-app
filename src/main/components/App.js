import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AppRegistry, StyleSheet, View, StatusBar, Platform, Text} from 'react-native';

import LoginView from './LoginView';
import Tabs from './Tabs';

const styles = {
    topSpacing:{
        height: 20,
        backgroundColor: '#fff'
    }
};

class App extends Component {
    render() {
        let viewToDisplay = this.props.loggedIn ? (<Tabs/>) : (<LoginView/>);

        return (<View style={{flex: 1}}>
            <View style={styles.topSpacing} />

            {viewToDisplay}
        </View>);
    }
}

function mapStateToProps(state) {
    return {
        loggedIn: state.login.loggedIn
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
