import axios from 'axios';
import {AsyncStorage, Alert, Platform} from 'react-native';
import qs from 'qs';
import {useMockData} from '../config';

let baseUrl = null,
    instance,
    extInstance;


// ************* //
//    INTERNAL   //
// ************* //

const setInstance = (data) => {
    instance = axios.create({
        baseURL: baseUrl,
        timeout: 10000,
        headers: {
            'Accept': 'application/json',
            'Authorization': data.access_token
        }
    });
};


instance = axios.create({
    baseURL: baseUrl,
    timeout: 10000,
    headers: {
        'Accept': 'application/json'
    }
});

const fetch = (type, data) => {
    if(useMockData){
        data.route = data.route + '.json';
    }
    console.log(baseUrl + data.route);

    instance[type](data.route, qs.stringify(data.data))
        .then(function(response) {
            data.success(response.data);
            if(data.done) {
                data.done(response.data);
            }
        })
        .catch(function(error) {
            data.fail(error);
            Alert.alert('Er is iets fout gegaan. Probeer het nogmaals.');
            if(data.done) {
                data.done(error);
            }
        });
};


// ************* //
//    EXTERNAL   //
// ************* //

const fetchExternal = (base, data) => {
    console.log(base + data.route);

    extInstance = axios.create({
        baseURL: base,
        timeout: 10000,
        headers: {
            'Accept': 'application/json'
        }
    });

    extInstance.get(data.route, qs.stringify(data.data))
        .then(function(response) {
            data.success(response.data);
            if(data.done) {
                data.done(response.data);
            }
        })
        .catch(function(error) {
            data.fail(error);
            Alert.alert('Er is iets fout gegaan. Probeer het nogmaals.');
            if(data.done) {
                data.done(error);
            }
        });
};

export const post = data => fetch('post', data);
export const get = data => fetch('get', data);
export const put = data => fetch('put', data);

export const getExternal = (base, data) => fetchExternal(base, data);

export const setHeaders = data => setInstance(data);
