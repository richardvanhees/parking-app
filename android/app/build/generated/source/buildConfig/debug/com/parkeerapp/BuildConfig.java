/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.parkeerapp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.parkeerapp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1112;
  public static final String VERSION_NAME = "1.1.12";
}
