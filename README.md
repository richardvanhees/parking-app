# Introduction

This is the development manual for the EMV Travel Mobile applications (iOS and Android). The mobile application is implemented using the React Native framework.

# Technology stack

These are the main technologies and libraries used in this project. Please take the time to read through the documentation of these libraries before starting to write/change code.

* [React Native](https://facebook.github.io/react-native/) for compiling the applcations to native apps
* [Redux](https://github.com/reactjs/redux), an implementation of the [Flux architecture](https://facebook.github.io/flux/docs/overview.html)
* Test tooling consists of
    * [Mocha](http://mochajs.org/) for writing tests, also see the following [page](https://gist.github.com/samwize/8877226) for some more details on writing tests
    * [Chai](http://chaijs.com/) for writing assertions
	* [Karma](http://karma-runner.github.io/0.12/index.html) for running them

## Prerequisites
Make sure the following programs are installed on your machine.

* [NodeJS](https://nodejs.org/) Use a recent version, like 4.5.0
* [npm](https://www.npmjs.com) Node package manager, which ships with Node.JS. Do check if you need to [update](https://docs.npmjs.com/getting-started/installing-node)
* A command line [Git client](https://git-scm.com/downloads) or a git client like [SourceTree](https://www.sourcetreeapp.com)
* [Firefox](http://getfirefox.com) (used for automated testing)
* For editing JavaScript:
    * [IntelliJ](https://www.jetbrains.com/idea/) latest version
    * Your favorite editor
* [XCode](https://developer.apple.com/xcode/) used for build tooling and some native implementation work
* [Android studio](https://developer.android.com/studio/index.html) used for build tooling and some native implementation work

## Installing external dependencies
Open a command line terminal, navigate to the `/app` directory, and type

    npm install

This downloads all external modules, and can take a while so be patient. Note that under Windows, you will probably see some error messages about a git command not working - you can ignore these, as they don't seem to be a problem in practice.

To run the iOS application:

    react-native run-ios

This should start the iOS simulator and will run the app.
To run the Android application, start the emulator first (through Android Studio, AVC manager, use SDK 23.0.1):

    react-native run-andriod


## JavaScript unit tests
The unit tests are located in the src/test folder. They can be run with the following command:

    npm test
    
## Android keystore
> Details can be found at https://facebook.github.io/react-native/docs/signed-apk-android.html.
> Keystore can be found in android/app.
* Name: parkeerapp.keystore
* Alias: parkeerapp
* Passwords: parkeerApp

