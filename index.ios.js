import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import store from './src/main/Store';
import Root from './src/main/Root';

class ParkeerApp extends Component {
  render() {
    return (
      <Root store={store}/>
    );
  }
}

AppRegistry.registerComponent('ParkeerApp', () => ParkeerApp);
