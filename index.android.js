import React, {Component} from 'react';
import {AppRegistry, StatusBar, View} from 'react-native';

import store from './src/main/Store';
import Root from './src/main/Root';

class ParkeerApp extends Component {
    render() {
        return (
            <View style={{flex:1, marginTop: -20}}>
                <Root store={store}/>
            </View>
        );
    }
}

AppRegistry.registerComponent('ParkeerApp', () => ParkeerApp);
